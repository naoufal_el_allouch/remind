angular.module('starter.services', [])

.factory('Todos', function($http) {
var url = "http://nelallouch.rmorpheus.enseirb-matmeca.fr/Remind/rest/";

var id;
var mail;
  return {
    all: function(mail) {
var method = url+"todos/"+mail;
      return  $http({ 
url:method,    
method : 'GET'
      })
.success(function(data) {

return data;
  });

 },
    get: 
function(id,data) {

    for(var i=0 ; i<data.length ; i++){
   var json = data[i]; 
if(json.id == id)
    return json;


}
    return false;

    
    
    },
    remove: function(id) 
      
{
var method = url+"todos/delete/"+id;
var i = 1;
$http({ 
url:method,    
method : 'GET'
      })
.success(function(data) {

})
.error(function(){
 
});

    },
add:function(newTodo,mail){
var method=url+"todos/add";

var yyyy = newTodo.deadline.getFullYear().toString();
   var mm = (newTodo.deadline.getMonth()+1).toString(); 
   var dd  = newTodo.deadline.getDate().toString();
   var deadline = (dd[1]?dd:"0"+dd[0])+"-"+(mm[1]?mm:"0"+mm[0]) +"-"+ yyyy ;    
    
    
$http({ 
url:method,    
method : 'POST',
 headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
    
transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    },
data: {'id': newTodo.id,'user':mail,'title':newTodo.title,'description':newTodo.description,'deadline':deadline,'state':newTodo.state}
      })
},
    
edit:function(todo){
var method=url+"todos/edit";

var yyyy = todo.deadline.getFullYear().toString();
   var mm = (todo.deadline.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = todo.deadline.getDate().toString();
   var deadline = (dd[1]?dd:"0"+dd[0])+"-"+(mm[1]?mm:"0"+mm[0]) +"-"+ yyyy ;    
    
    
$http({ 
url:method,    
method : 'POST',
 headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
    
transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    },
data: {'id': todo.id,'user':todo.user,'title':todo.title,'description':todo.description,'deadline':deadline,'state':todo.state}
      })
},
getMail:function(){
 return mail   
},
      
signup:function(newUser){
var method = url+"users/add";
var mail = newUser.mail;
return $http({ 
url:method,    
method : 'POST',
 headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
    
transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    },
data: {'mail':newUser.mail,'password':newUser.pass}
      })
.success(function(data) {
return data;
  });

    
},
      
      
signin:function(user){
var method = url+"users/signin";
var mail = user.mail;
return $http({ 
url:method,    
method : 'POST',
 headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
    
transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    },
data: {'mail':user.mail,'password':user.pass}
      })
.success(function(data) {
return data;
  });

    
}
      
  };
});
