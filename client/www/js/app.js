angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ionic-datepicker'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(['$ionicConfigProvider', function($ionicConfigProvider) {

//$ionicConfigProvider.tabs.position('bottom'); //other values: top
$ionicConfigProvider.views.maxCache(0);
}])
.config(function($stateProvider, $urlRouterProvider) {


  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive

    .state('home', {
    url: "/home",
    templateUrl: "/android_asset/www/home.html",
controller:'UsersCtrl'
  })  
      
      
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "/android_asset/www/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: '/android_asset/www/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.todos', {
      url: '/todos',
      views: {
        'tab-todos': {
          templateUrl: '/android_asset/www/todos-list.html',
          controller: 'TodosCtrl'
        }
      }
    })
    .state('tab.todo-detail', {
      url: '/todos/:id',
      views: {
        'tab-todos': {
          templateUrl: '/android_asset/www/todo-detail.html',
          controller: 'TodoDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: '/android_asset/www/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});
