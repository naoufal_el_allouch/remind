angular.module('starter.controllers', [])


.controller('SinIn', function($scope,Todos,$stateParams,$rootScope,$ionicModal) {

    
$scope.currentDate = new Date($rootScope.deadline);
 // /android_asset/www
       
    
  $ionicModal.fromTemplateUrl('/android_asset/www/templates/signin.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
})


.controller('SignInForm', function($scope,Todos,$stateParams,$rootScope,$ionicModal) {

    
$scope.currentDate = new Date($rootScope.deadline);
      
 // /android_asset/www
       
    
  $ionicModal.fromTemplateUrl('/android_asset/www/templates/signin.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
})

.controller('SignUpForm', function($scope,Todos,$stateParams,$rootScope,$ionicModal) {

    
$scope.currentDate = new Date($rootScope.deadline);
      
 // /android_asset/www
       
    
  $ionicModal.fromTemplateUrl('/android_asset/www/templates/signup.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
})

.controller('AddTodo', function($scope,Todos,$stateParams,$rootScope,$ionicModal) {

    
$scope.currentDate = new Date($rootScope.deadline);
      //console.log('HomeTabCtrl');
 // /android_asset/www
       
    
  $ionicModal.fromTemplateUrl('/android_asset/www/templates/addForm.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
})




.controller('EditTodo', function($scope,Todos,$stateParams,$rootScope,$ionicModal) {

    
$scope.currentDate = new Date($rootScope.deadline);
      //console.log('HomeTabCtrl');
 // /android_asset/www
       
    
  $ionicModal.fromTemplateUrl('/android_asset/www/templates/editForm.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
})

.controller('TodosCtrl', function($scope,Todos,$http,$state,$ionicModal,$window,$rootScope) {
    

//$scope.currentDate =

 // /android_asset/www

var mail = "root@gmail.com";
Todos.all(mail)
.then(function(response){

$scope.todos = response.data;  
    
    
});  
    
$scope.add=function(newTodo){

Todos.add(newTodo,mail);

$window.location.reload(true);  

}

$scope.edit=function(todo){
Todos.edit(todo);

$window.location.reload(true);

    
};
$scope.remove=function(id){

Todos.remove(id);
$state.go($state.current, {}, {reload: true}); 

};

})

.controller('UsersCtrl', function($scope,Todos,$http,$state,$ionicModal,$window,$rootScope) {

$scope.signin=function(user){
console.log(user);
if(user.mail != "" || user.pass !="" ){
    
Todos.signin(user)
.then(function(response){
if(response.data.connected == true ){
 $state.go('tab.todos');

$window.location.reload(true);
}
    
else{
$scope.erroru = "Identifiants incorrects";
}
    
});

    
}
    else
 $scope.errord="Veuillez compléter les champs manquants";
}; 
    
$scope.signup=function(newUser){
    
if(newUser.pass == newUser.passcf){
    
Todos.signup(newUser)
.then(function(response){
if(response.data.found == false && response.data.registred == true){
 $state.go('tab.todos');

$window.location.reload(true);
}
    
else{
$scope.errorexist = "Email déjà utilisé";
}
    
});

    
}
    else
 $scope.errorpass="Mots de passes différents";
}; 

 // /android_asset/www

})
/*
.controller('ModalCtrl', function($scope, $ionicModal) {
})*/

.controller('TodoDetailCtrl', function($scope,$stateParams, Todos) {


    

    var mail = "root@gmail.com";
    
Todos.all(mail)
.then(function(response){


var    todo = Todos.get($stateParams.id,response.data);

$scope.todo = todo;
});    

    
    
})

.controller('AccountCtrl', function($scope,$state) {
$scope.disconnect=function(){
    
 $state.go('home');   
}
});
