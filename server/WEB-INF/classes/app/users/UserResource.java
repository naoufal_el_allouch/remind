package app.users;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.NoSuchElementException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("users")
public class UserResource {

	/*
	 * 	Authentification
	 */	
	@Path("signin")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public BasicDBObject signIn(@FormParam("mail")String mail ,@FormParam("password")String password) throws UnknownHostException{

		MongoClient mongoClient = new MongoClient( "localhost" );
		
		DB db = mongoClient.getDB("local");
		DBCollection coll = db.getCollection("Users");	
		

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("mail",mail);
		whereQuery.put("password",password);
		
		
		BasicDBObject response1 = new BasicDBObject();

		DBCursor cursor = coll.find(whereQuery);
		try{
		@SuppressWarnings("unused")
		BasicDBObject obj = (BasicDBObject) cursor.next();
		}
		catch(NoSuchElementException e){
			
			response1.put("connected",false);
			return response1;
		}
		
		response1.put("connected",true);

		return response1;

	}
	
	
	/*
	 * 	Inscription
	 */	
	@Path("add")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public BasicDBObject addUser(@FormParam("mail")String mail ,@FormParam("password")String password) throws UnknownHostException{

		MongoClient mongoClient = new MongoClient( "localhost" );
		
		DB db = mongoClient.getDB("local");
		DBCollection coll = db.getCollection("Users");	

		BasicDBObject user = new BasicDBObject();
		user.put("mail",mail);
		user.put("password",password);
		
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("mail", mail);
		
		BasicDBObject response = new BasicDBObject();

		DBCursor cursor = coll.find(whereQuery);
		try{
		@SuppressWarnings("unused")
		BasicDBObject obj = (BasicDBObject) cursor.next();
		}
		catch(NoSuchElementException e){
			response.put("found",false);
			coll.insert(user);
			response.put("registred",true);
			return response;
		}
		
		response.put("found",true);
		response.put("registred",false);
		return response;

	}
	
	
}
