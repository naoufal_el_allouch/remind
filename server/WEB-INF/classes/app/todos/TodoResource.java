package app.todos;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("todos")

public class TodoResource {
	
	/*
	 * 	Ajouter un todo
	 */	
	@Path("add")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	public Todo addTodo(@FormParam("user")String user, @FormParam("title")String title, @FormParam("description")String description, @FormParam("deadline")String deadline) throws UnknownHostException{

		MongoClient mongoClient = new MongoClient( "localhost" );
		
		DB db = mongoClient.getDB("local");
		DBCollection coll = db.getCollection("Todos");	
		int id = (int) (coll.count()+1);	
		String state = "En cours";
		app.todos.Todo todo = new Todo(id , user , title , description , deadline ,state);

		BasicDBObject document = new BasicDBObject();

		document.put("id",id);
		document.put("user",user);
		document.put("title",title);
		document.put("description",description);
		document.put("deadline",deadline);
		document.put("state",state);
		coll.insert(document);


		return todo;
	}
	
	/*
	 * 	Editer un todo
	 */	
	@Path("edit")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Todo editTodo(@FormParam("id")int id ,@FormParam("user")String user, @FormParam("title")String title, @FormParam("description")String description, @FormParam("deadline")String deadline, @FormParam("state")String state) throws UnknownHostException{

		MongoClient mongoClient = new MongoClient( "localhost" );
		
		DB db = mongoClient.getDB("local");
		DBCollection coll = db.getCollection("Todos");	

		Todo todo = new Todo(id , user , title , description , deadline ,state);
		BasicDBObject document1 = new BasicDBObject();

		document1.put("id",id);
		document1.put("user",user);
		document1.put("title",title);
		document1.put("description",description);
		document1.put("deadline",deadline);
		document1.put("state",state);

		
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("id", id);
		
		coll.update(whereQuery,document1);

		return todo;
	}
	
	/*
 	* 	Suppression de todo(id)
 	*/	
	@GET
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public BasicDBObject getContact(@PathParam("id") int id) throws UnknownHostException{
		MongoClient mongoClient = new MongoClient( "localhost" );
	
		DB db = mongoClient.getDB("local");
		DBCollection coll = db.getCollection("Todos");
	
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("id", id);
		DBCursor cursor = coll.find(whereQuery);
		
		BasicDBObject obj = (BasicDBObject) cursor.next();
		
		coll.remove(obj);

		return obj;
	}
	
	/*
 	* 	Liste de tous les todos(mail)
 	*/		
	
	@GET
	@Path("{mail}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Todo> getContact(@PathParam("mail") String mail) throws UnknownHostException, ParseException{
		MongoClient mongoClient = new MongoClient( "localhost" );
	
		DB db = mongoClient.getDB("local");
		DBCollection coll1 = db.getCollection("Todos");

		ArrayList<Todo> todos = new ArrayList<Todo>();	
	// Chercher la collection Todos dans la bdd
		BasicDBObject whereQuery1 = new BasicDBObject();
		whereQuery1.put("user", mail);
		
		DBCursor cursor = coll1.find(whereQuery1);
		
		while(cursor.hasNext()) {
			   BasicDBObject obj = (BasicDBObject) cursor.next();

			   int id =Integer.parseInt(obj.getString("id"));
			    String user = obj.getString("user");
			    String title = obj.getString("title");
				String description = obj.getString("description");
				String deadline = obj.getString("deadline");

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				java.util.Date dateA = new java.util.Date(); 

					String today = formatter.format(dateA);
					String newState;
					
				    java.util.Date auj = formatter.parse(today);
				    java.util.Date deadl = formatter.parse(deadline);
				    
//					System.out.println(auj.getTime());
//					System.out.println(deadl.getTime());
				
				
					if(auj.getTime() < deadl.getTime())
					newState = "En cours"; 
					
					else 
					newState = "Terminé";
editTodo(id ,user, title,  description,  deadline, newState);

todos.add(new Todo(id , user , title , description , deadline , newState));

				 
			 	
			   
		}

		return todos;
	}

}