package app.todos;

public class Todo {

	private int id;
	private String user;
	private String title;
	private String description;
	private String deadline;
	private String state = "En cours";

	public Todo(){
		super();
	}
	
	
	public Todo(int id, String user, String title, String description,
			String deadline, String state) {
		this.id = id;
		this.user = user;
		this.title = title;
		this.description = description;
		this.deadline = deadline;
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
